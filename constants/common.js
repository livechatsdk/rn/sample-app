import { PixelRatio, Platform } from "react-native";
//import { AppLogger } from "../utils/Logger";

let pixelRatio = PixelRatio.get();
let fontScale = PixelRatio.getFontScale();

console.log(`Pixel Ratio for this device is: ${pixelRatio}`);
console.log(`Font Scale for this device is: ${fontScale}`);

export const COLORS = {
    RED: '#ff3f3f',
    BLACK: '#0c0c0c',
    WHITE: '#ffffff',
    BLUE_MEDIUM: '#1976d2',
    BLUE_DARK: '#004ba0',
    BLUE_LIGHT: '#75aeff',
    BLUE_LIGHT_2: '#f4faff',
    PURPLE_DARK: '#742b91',
    PURPLE_LIGHT: '#e6a8ff',
    PURPLE_LIGHT_2: '#ffffff',
    PURPLE_MEDIUM: '#9135b5',
    GREY_LIGHT: '#d9d9d9',
    GREY_50: '#fafafa',
    GREY_100: '#f5f5f5',
    GREY_400: '#818181',
    GREY_LIGHT_GREEN: '#d5d8d4',
    BLACK_TITLE_TEXT: '#252525',
    PRIMARY_COLOR: '#3F2AA8',
    PRIMARY_BACKGROUND_COLOR: '#E4DEFF',
    GREY_BACKGROUND_COLOR: '#EFEFEF',
    SECOND_COLOR: '#FF7654',
    LIGHT_GREEN: `#29c93f`,
    ORANGE: `#f3ae6f`,

    BG: '#7343DF',
    DARK_TXT_BG: '#5E36B1',
    DARK_LABEL: '#BFB9CB',
    DARK_PLACEHOLDER: '#8B61EA',
    FONT_COLOR: '#252525',
    FONT_SUB_COLOR: '#9B91D3',
    ONLINE: '#06d838',
    OFFLINE: '#f4faff',
    BUSY: '#ff3f3f',

    CHAT_BUBBLE_WRAPPER_BACKGROUND: '#c9c6f8',
    CHAT_BUBBLE_WRAPPER_BACKGROUND_DARK: '#c3bae9',
    CHAT_BUBBLE_WRAPPER_SHADOW: '#c4bdb6fc',

    ONLINE_STATUS_FLEX: 0.3,
    STATUS_BORDER_COLOR: '#F9F9FC',
    CHAT_RIGHT: '#F3F1FB',

    DEFAULT_BG: '#ffffff',
    ERROR_BG: '#ff444b',
    INFO_BG: '#3F2AA8',
    ACTIVE_CALL: '#23d46f',

    BUTTON_END: '#FF5B5B',

    RIGHT_CHAT_BG: '#F3F1FB',
    RIGHT_REPLAY_BG: '#c7c1e9',
    LEFT_CHAT_BG: '#f5f5f5',

    TITLE: 18,
    TITLE_WEIGHT: Platform.OS === "ios" ? "800" : "bold",
};

//This is done due to an issue in android with font weights
//https://github.com/facebook/react-native/issues/26193
export const FONT_WEIGHTS = {
    100: '100',
    200: Platform.OS === "ios" ? "200" : "100",
    300: Platform.OS === "ios" ? "300" : "100",
    400: Platform.OS === "ios" ? "400" : "100",
    500: Platform.OS === "ios" ? "500" : "bold",
    600: Platform.OS === "ios" ? "600" : "bold",
    700: Platform.OS === "ios" ? "700" : "bold",
    800: Platform.OS === "ios" ? "800" : "bold",
    900: Platform.OS === "ios" ? "900" : "bold",
}

// Generic method used to scale fonts based on the pixel ratio
export const getFontSize = fontSize => {
    try {
        // if (pixelRatio <= 2)
        //     fontSize -= 1;
        // else if (pixelRatio <= 1)
        //     fontSize -= 2;

        return (fontSize / fontScale);

        //return fontSize;
    } catch (error) {
        console.log(``, error);
    }
}