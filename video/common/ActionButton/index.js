'use strict';
import {Image, Text, TouchableOpacity, View, Button} from "react-native";
import {styles} from "./styles";
import React from "react";

const consts = require('../../../constants/common');

const ActionButton = (props) => (
    <>

        {props.type === 'micBtn' &&
        <View style={[styles.actionButtonWrapper, styles[props.isActive]]}>
            {props.isActive !== 'active' &&
            <Image style={styles.buttonIcon} source={require('../../img/micBlack.png')}/>
            }

            {props.isActive === 'active' &&
            <Image style={styles.buttonIcon} source={require('../../img/micWhite.png')}/>
            }
        </View>
        }

        {props.type === 'videoBtn' &&
        <View style={[styles.actionButtonWrapper, styles[props.isActive]]}>
            {props.isActive !== 'active' &&
            <Image
                style={styles.buttonIcon}
                source={require('../../img/videoEnableBlack.png')}/>
            }

            {props.isActive === 'active' &&
            <Image style={styles.buttonIcon} source={require('../../img/videoEnableWhite.png')}/>
            }
        </View>
        }

        {props.type === 'chatBtn' &&
        <View style={[styles.actionButtonWrapper, styles[props.isActive]]}>
            <Image
                style={styles.buttonIcon}
                source={require('../../img/chatBack.png')}/>
        </View>
        }

        {props.type === 'speakBtn' &&
        <View style={[styles.actionButtonWrapper, styles[props.isActive]]}>
            {props.isActive !== 'active' &&
            <Image
                style={styles.buttonIcon}
                source={require('../../img/spekerBlack.png')}/>
            }

            {props.isActive === 'active' &&
            <Image style={styles.buttonIcon} source={require('../../img/spekerWhite.png')}/>
            }
        </View>
        }
    </>
);

export default ActionButton;
