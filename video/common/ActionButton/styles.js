import {StyleSheet} from 'react-native';

const consts = require('../../../constants/common');

export const styles = StyleSheet.create({
    actionButtonWrapper: {
        width: 50,
        height: 50,
        backgroundColor: consts.COLORS.WHITE,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20
    },
    buttonIcon : {
    },
    active : {
        backgroundColor: consts.COLORS.PRIMARY_COLOR,
    }
});

