'use strict';
import {Image, Text, TouchableOpacity, View, Button} from "react-native";
import {styles} from "./styles";
import React from "react";

const consts = require('../../../constants/common');

const EndButton = (props) => (
    <>
        <View style={styles.endButtonWrapper}>
            <Image
                style={styles.endButtonIcon}
                source={require('../../img/endCall.png')}/>
        </View>
    </>
);

export default EndButton;
