import {StyleSheet} from 'react-native';

const consts = require('../../../constants/common');

export const styles = StyleSheet.create({
    endButtonWrapper: {
        width: 75,
        height: 75,
        backgroundColor: consts.COLORS.BUTTON_END,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30
    },
    endButtonIcon : {
    }
});

