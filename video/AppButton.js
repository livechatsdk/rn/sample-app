import React, {Component} from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
const consts = require(`../constants/common`)

export default class AppButton  extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        var istyle = {  borderColor:'#a0d0ff', borderRadius:10 };

        if( this.props.hasborder )
        {
            istyle.borderWidth = 1;
        }

        return (

            <TouchableOpacity style={istyle} onPress={this.props.onPress} onLongPress={this.props.onLongPress} >
                    <Text style={{color:"#00a0ff", fontSize:consts.getFontSize(17), margin:5}}>
                        { " " + (this.props.title) + " " }
                    </Text>
            </TouchableOpacity>

        );
    }
}

