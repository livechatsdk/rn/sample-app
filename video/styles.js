import {StyleSheet} from 'react-native';

const consts = require('../constants/common');

export const styles = StyleSheet.create({
    callActionBtnWrapper: {
        flexDirection: 'column',
        position: 'absolute',
        width: '100%',
        height: 25,
        bottom: '25%',
    },
    endBtnWrapper: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    }, callActionWrp: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 40
    },
    actionBtnItem: {
        width: 50,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    connectingProfileWrapper: {
        width: '100%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '60%',
        height: 200,

    },
    userImage: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 140,
        height: 140,
        backgroundColor: consts.COLORS.WHITE,
        borderRadius: 100
    },
    selfViewImage: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 80,
        height: 120,
        backgroundColor: consts.COLORS.WHITE,
        borderRadius: 10,

    },
    profileTextWrapper: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '5%',
    },
    callerName: {
        color: consts.COLORS.BLACK,
        fontSize: 30,
        fontWeight: '800',
    },
    callState: {},
    selfViewWrapper: {
        position: 'absolute',
        marginLeft: '10%',
        marginTop: 50
    },
    videoMainWrapper: {
        flex: 1,
        backgroundColor: consts.COLORS.WHITE,
    }
});
