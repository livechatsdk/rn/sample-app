
import React, {Component} from 'react';
import { Platform, ScrollView, Dimensions, Text, View, Button, Image, TouchableOpacity, requireNativeComponent } from 'react-native';
const consts=require(`../constants/common`);

const Lvchatvideo = requireNativeComponent('RNVideoView' , null);

global.callstatus = "Connecting ..";

function logi( msg )
{
    global.log.func( "info", "CallScreen:" + msg );
    return msg;
}

function logw( msg )
{
    global.log.func( "warn", "CallScreen:" + msg );
    return msg;
}

function loge( msg )
{
    global.log.func( "error", "CallScreen:" + msg );
    return msg;
}

function logd( msg )
{
    global.log.func( "debug", "CallScreen:" + msg );
    return msg;
}

export default class CallScreenEx extends Component
{
    navi;

    state = {
        callsession:{ currentState:{ usertxt:"" } },

    };

    // avcalled = false;
    // orientation = "U";

    orgSetState;

    constructor( props )
    {
        super(props);
        this.muteImg = require('./imgEx/mute.png');
        this.unMuteImg = require('./imgEx/unmute.png');
        this.speakerImg = require('./imgEx/speaker.png');
        this.noSpeakerImg = require('./imgEx/nospeaker.png');

        navi = props.navigation;
        this.myid = "sc.audio";
        global.calls.callCreateHandlers[this.myid] =  this.onNewCallSession;

        this.reconnectImg = require('./imgEx/reconnect.png');
        this.noreconnectImg = require( './imgEx/noreconnect.png' );

        this.orgSetState = this.setState;
        this.setState = ( f ) => {
            this.orgSetState( ( s ) =>
                {
                    return global.state.CallScreen = f(s);
                }
            );
        }

        if( global.state.CallScreen )
        {
            this.state = global.state.CallScreen;
        }
        else
        {
            this.reset();
            global.state.CallScreen = this.state;
        }

        this.onNewCallSession();

        console.log( "CallScreen created" );
    }

    reset = () =>
    {
        logd( "Resetting .." );
        
        this.state.reconnectstyle = { borderRadius:20 };
        this.state.mutestyle = { borderRadius:20 };
        this.state.speakerstyle = { borderRadius:20 };
        this.state.ismuted = false;
        this.state.isCamEnabled = true;
        this.state.isvideo = true;
        this.state.activeMuteImg = this.unmuteImg;
        this.state.activeSpeakerImg = this.noSpeakerImg;
        this.state.activeMuteImg = this.unMuteImg;
        this.state.activeReconnectImg = this.noreconnectImg;
        this.state.remoteViewIndex = -1;
        this.state.localViewIndex = -1;
        this.state.orientation = "U";
        this.state.isspeaker = false;
        this.state.remoteViewIndexList = [];

        this.setState( p=>{ return p; } );
    }

    onLayout = ( e ) =>
    {
        const {width, height} = Dimensions.get('window');

        if( width > height )
            this.state.orientation = "L"; // landscape
        else if( height > width )
            this.state.orientation = "P"; // potrait
        else
            this.state.orientation = "U"; // unknown

        logi( "new orientation : " + this.state.orientation );
    }

    onNewCallSession=()=>
    {

        console.log( "onNewCallSession " );

        var callsession = global.callsession;
        this.state.callsession = callsession;
        var csStateEvents = callsession.StateEvent;
        var csCommonEvents = callsession.CommonEvent;

        // state events

        callsession.AddHandler( csStateEvents.MEDIA_REQ_RECEIVED, this.myid,
            ( mode ) => {
                this.reset();
                this.setState( p=>{ return p; } );
            }
        );

        callsession.AddHandler( csStateEvents.MEDIA_REQ_SENT, this.myid,
            ( mode ) => {
                this.reset();
                this.setState( p=>{ return p; } );
            }
        );

        callsession.AddHandler( csStateEvents.INCALL_MEDIA, this.myid,
            ( mode )=>{
                global.pushNav( this.props.navigation, "Call" );

                this.setState( s =>
                {
                    s.activeReconnectImg = this.noreconnectImg;
                    s.reconnectstyle.backgroundColor = undefined;
                    return s;
                } );
            }
        );

        callsession.AddHandler( csStateEvents.ENDED, this.myid,
            (reason)=>{
              //this.props.navigation.navigate("ChatThread", {meta: global.chatMeta});
              global.goBack( this.props.navigation );
              this.reset();
            }
        );

        callsession.AddHandler( csCommonEvents.VIDEO_RECEIVED, this.myid,
            ( index , type, source, owner )=>{

            logi( "onvideo index=" + index + ", type=" + type );

            this.state.isvideo = true;

             if( type == "remote" )
             {
                this.state.remoteViewIndexList.push( parseInt(index) );
                this.state.remoteViewIndex = index;
                logd( "received remoteViewIndexList = " + this.state.remoteViewIndexList.toString() );
             }
             else if( type == "local" )
             {
                 this.state.localViewIndex = index;
             }
             else
             {
                 loge( "unknown type" );
                 return;
             }

             global.pushNav( this.props.navigation, "Call");
             this.setState( p=>{ return p; } );
            }
        );

        callsession.AddHandler( csCommonEvents.VIDEO_DROPPED, this.myid,
            ( index , side, source, owner )=>{
                if( side == "remote" )
                {
                    var arrIndex = this.state.remoteViewIndexList.lastIndexOf( parseInt(index) );
                    delete this.state.remoteViewIndexList[arrIndex];
                    this.setState( p=>{ return p; } );
                }
            }
        );

        callsession.AddHandler( csStateEvents.RECONNECTING_MEDIA, this.myid,
            ( mode )=>{

                this.setState( s=>{
                    s.activeReconnectImg = this.reconnectImg;
                    s.reconnectstyle.backgroundColor = 'white';
                    return s;
                } );
            }
        );

        callsession.AddHandler( csStateEvents.REINCALL_MEDIA, this.myid,
            ( mode )=>{

                this.setState( s => {
                    s.activeReconnectImg = this.noreconnectImg;
                    s.reconnectstyle.backgroundColor = undefined;
                    return s;
                } );
            }
        );
    }

    componentWillMount() {
        /* mve
        this.avconn.onSignallingMessage = (message) => {
            global.calls.av2txt.sigMessage( message );
        }
        */
    }


    endVoiceCall_Clicked = () =>
    {
       global.callsession.End( "Customer ended audio-video session" );
       this.props.navigation.navigate( 'Start' );
    }


    mute_Clicked = () => 
    {
        global.callsession.Mute( this.state.ismuted = ( !this.state.ismuted ) );

        if( this.state.ismuted )
        {
            this.state.activeMuteImg = this.muteImg;
            this.state.mutestyle.backgroundColor = 'white';
        }
        else
        {
            this.state.activeMuteImg = this.unMuteImg;
            this.state.mutestyle.backgroundColor = undefined;
        }

        this.setState( s=>{ return s; } );
    }

    reconnect_Clicked = () => 
    {
        //global.callsession.Reconnect();
        global.callsession.VideoEnabled( this.state.isCamEnabled = ( !this.state.isCamEnabled ) );

        if( this.state.isCamEnabled )
        {
            this.state.activeReconnectImg = this.noreconnectImg;
            this.state.reconnectstyle.backgroundColor = undefined;
        }
        else
        {
            this.state.activeReconnectImg = this.reconnectImg;
            this.state.reconnectstyle.backgroundColor = 'white';
        }

        this.setState( p=>{ return p; } );
    }


    speaker_Clicked = () => {

        global.callsession.LoudSpeaker( this.state.isspeaker = ( !this.state.isspeaker ) );

        if( this.state.isspeaker )
        {
            this.state.activeSpeakerImg = this.speakerImg;
            this.state.speakerstyle.backgroundColor = 'white';
        }
        else
        {
            this.state.activeSpeakerImg = this.noSpeakerImg;
            this.state.speakerstyle.backgroundColor = undefined;
        }

        this.setState( s=>{ return s; } );
    }

    test_click = () => {
        // logi( "g remoteViewIndex = " + global.state.CallScreen.remoteViewIndex );
        this.setState( p=>{ return p; } );
    }


    textchat_Clicked = () => {

        if( global.state.StartScreen )
        {
            global.pushNav( this.props.navigation, "Logs" );
            return;
        }

        if( global.chatMeta )
            this.props.navigation.navigate("ChatThread", {meta: global.chatMeta});
        else
            this.props.navigation.navigate("ContactList" );
    }

    /*
    remoteViewIndex = -1;
    localViewIndex = -1;
    isvideo = false;
    */

    usercust = [] ;

    render()
    {
        useragent = [];
        console.log(  "Rendering CallScreen . ." );

        var topflex;

        if( this.state.isvideo )
        {
            /*
            for( var ind of this.state.remoteViewIndexList )
            {
                useragent.push ( <Lvchatvideo viewindex={ind} style={{ backgroundColor: 'blue', flex:1, alignSelf:'stretch' }} /> );
            }
            */

           this.state.remoteViewIndexList.forEach( (ind) =>
           {
               useragent.push ( <Lvchatvideo viewindex={ind} style={{ flex:1, alignSelf:'stretch' }} /> );
           });

            this.usercust[0] = ( <Lvchatvideo viewindex={this.state.localViewIndex} style={{ top:15, left:0, width:150, height:150, position: 'absolute' }} /> );
            topflex = 3;
        }
        else
        {
            useragent = (<Image resizeMode="center" resizeMethod="scale" source={require('./imgEx/agent.png')}/>);
            topflex = 2;
        }


        return (

            <View style={{flex: 1, flexDirection: 'column', backgroundColor: 'green' }}>

                <View  style={{flexDirection:'column', justifyContent:'center', alignItems:'center', flex:topflex }}>

                {useragent}

                {this.usercust[0]}

                    <View style={{width:20, height:5}}/>
                    <Text style={{ fontWeight:"bold", color:"white", fontSize: consts.getFontSize(20) }} >{global.contact.name}</Text>
                    <Text style={{ color:"white", fontSize: consts.getFontSize(15) }}> {this.state.callsession.currentState.usertxt} </Text>

                </View>

                <TouchableOpacity onPress={ this.test_click } >
                            <Text></Text>
                  </TouchableOpacity>

                <View style={{ flex:1,flexDirection:'row'}}>

                  <View style={{width:20, height:10}}/>

                  <View style={{  flex:1, alignItems: 'stretch', flexDirection:'row', borderRadius:20 }}>
                    <View style={{ flex:1, alignItems: 'center' , flexDirection:'column', justifyContent:'space-between' }} >

                        <TouchableOpacity style={ this.state.mutestyle } onPress={ ()=>{ this.mute_Clicked(); } } >
                            <Image style={{ width:60, height:60 }} source={ this.state.activeMuteImg } />
                        </TouchableOpacity>

                        <TouchableOpacity style={ this.state.speakerstyle } onPress={ ()=>{ this.speaker_Clicked(); } } >
                            <Image style={{ width:60, height:60 }} source={ this.state.activeSpeakerImg } />
                        </TouchableOpacity>

                    </View>

                    <TouchableOpacity style={{ aspectRatio:1, maxWidth:300, maxHeight:100, alignSelf:"center", alignItems:"center", borderWidth:10, borderColor:'white', borderRadius:100,
                      backgroundColor: 'white', flex:1, flexDirection:'column'}} onPress={ ()=>{this.endVoiceCall_Clicked();} }>
                        <Image style={ {width:50, height:50 }} resizeMode="contain" resizeMethod="scale" source={require('./imgEx/callend-red.png')}/>
                    </TouchableOpacity>

                    <View style={{ alignItems:'center', flex:1, flexDirection:'column', justifyContent:'space-between' }}>

                        <TouchableOpacity onPress={ this.textchat_Clicked } >
                            <Image style={{ width:50, height:50 }} source={require('./imgEx/chat.png')} />
                        </TouchableOpacity>

                        <TouchableOpacity style={ this.state.reconnectstyle } onPress={ ()=>{ this.reconnect_Clicked(); } } >
                            <Image style={{ width:30, height:30 }} source={ this.state.activeReconnectImg } />
                        </TouchableOpacity>

                    </View>

                  </View>


                  <View style={{width:20, height:10}}/>

                </View>

                <View style={{flex:0.2}} />

    <TouchableOpacity delayLongPress={3000}

        onLongPress={()=>{ global.pushNav( this.props.navigation, 'Logs' ); } }

        style={{ position: 'absolute', top:50, right:5, height: 100, width:70,

        /* backgroundColor:'orange'*/ }} >

    </TouchableOpacity>

            </View>

        );

    }

    styles = {
        bgtop : {
            color: 'Gainsboro'
        }
    }
}

