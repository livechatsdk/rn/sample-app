import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, FlatList, Clipboard} from 'react-native';
import {NavigationActions} from 'react-navigation';

import AppButton from './AppButton';

export default class LogScreen extends React.Component {

    constructor(props) {
        super(props);
        this.txtList = global.log.data;
        this.state = {count: 0};

        var c = [];
        c["error"] = c["Error"] = "pink";
        c["warn"] = c["Warn"] = "yellow";
        c["info"] = c["Info"] = "white";
        c["debug"] = c["Debug"] = "white";
        c["emph"] = c["Emph"] = "lightskyblue";
        c["msgout"] = c["Msgout"] = "palegreen";
        c["msgin"] = c["msgin"] = "palegreen";

        this.itemBgcolor = c;

        global.log.outfunc = this.addLog;

        //construct navigation
        //this.props.navigation.navigate("av");

        console.log("log screen constructed");
    }

    addLog = (type, msg) => {

        this.setState(p => {
            return p;
        });
    }

    onClick = () => {
        this.addLog('error', 'hello');
    }

    _renderItem = ({item, index}) => {

        try {
            var istyle = {};

            var bgcolsel = item.type;

            if (item.msg) {
                if (item.msg.endsWith('*'))
                    bgcolsel = "emph";
                else if (item.msg.endsWith('->'))
                    bgcolsel = "msgout";
                else if (item.msg.endsWith('<-'))
                    bgcolsel = "msgin";
            } else {
                bgcolsel = 'error';
                item.msg = "LOGERR: missing msg : " + JSON.stringify(item)
            }

            istyle.backgroundColor = this.itemBgcolor[bgcolsel];

            return <Text style={istyle}>{item.msg}</Text>;
        } catch (e) {
            console.log('log _renderItem error.....' +  e);
        }
    }

    goBack = () => {
        //if( this.props.navigation.state.params )
        //    this.props.navigation.navigate('text');
        // else

        // this.props.navigation.navigate( 'Login' );

        global.goBack(this.props.navigation);

    }

    clear = () => {
        this.txtList.length = 0;
        this.addLog(null, null);
    }

    copy = () => {
        var cpstr = "";
        this.txtList.forEach(element => {
            cpstr += (element.type + " : " + element.msg + "\n");
        });
        Clipboard.setString(cpstr);
    }

    render() {

        //console.log( "Rendering LogScreen count = " + this.txtList.length );

        return (

            <View style={{
                backgroundColor: 'white',
                flex: 1,
                flexDirection: 'column',
                alignItems: 'stretch',
                justifyContent: 'flex-start',
                margin: 10
            }}>

                <View style={{height: 20}}/>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start'}}>
                    <AppButton hasborder='1' title="< Back" onPress={() => {
                        this.goBack();
                    }}/>
                    <AppButton hasborder='1' title="Clear" onPress={() => {
                        this.clear();
                    }}/>
                    <AppButton hasborder='1' title="Copy" onPress={() => {
                        this.copy();
                    }}/>
                </View>

                <FlatList data={this.txtList} extraData={this.txtList.length}
                          renderItem={this._renderItem}/>

            </View>
        );
    }

}

