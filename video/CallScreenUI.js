import React, {Component} from 'react';
import {
    Platform,
    ScrollView,
    Dimensions,
    Text,
    View,
    Button,
    Image,
    TouchableOpacity,
    requireNativeComponent
} from 'react-native';
import {styles} from "./styles";

import {EndButton, ActionButton} from './common';


function logi(msg) {
    global.log.func("info", "CallScreen:" + msg);
    return msg;
}

function logw(msg) {
    global.log.func("warn", "CallScreen:" + msg);
    return msg;
}

function loge(msg) {
    global.log.func("error", "CallScreen:" + msg);
    return msg;
}

function logd(msg) {
    global.log.func("debug", "CallScreen:" + msg);
    return msg;
}

export default class CallScreenUI extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {

    }

    render() {
        return (
            <>

                <View style={styles.selfViewWrapper}>
                    <View>
                        <Image
                            resizeMode='cover'
                            style={[styles.selfViewImage]}
                            source={{uri: 'https://source.unsplash.com/XHVpWcr5grQ'}}/>
                    </View>
                </View>

                <View style={styles.connectingProfileWrapper}>
                    <View>
                        <Image
                            resizeMode='cover'
                            style={[styles.userImage]}
                            source={{uri: 'https://source.unsplash.com/kvKSL7B6eTo'}}/>
                    </View>
                    <View style={styles.profileTextWrapper}>
                        <Text style={styles.callerName}>Allie Butler</Text>
                        <Text style={styles.callState}>Connecting...</Text>
                    </View>
                </View>

                <View style={styles.callActionBtnWrapper}>
                    <View style={styles.endBtnWrapper}>
                        <EndButton/>
                    </View>
                    <View style={styles.callActionWrp}>
                        <View style={styles.actionBtnItem}>
                            <ActionButton
                                isActive={'active'}
                                type={'micBtn'}
                            />
                        </View>

                        <View style={styles.actionBtnItem}>
                            <ActionButton
                                isActive={''}
                                type={'videoBtn'}
                            />
                        </View>

                        <View style={styles.actionBtnItem}>
                            <ActionButton
                                isActive={''}
                                type={'chatBtn'}
                            />
                        </View>

                        <View style={styles.actionBtnItem}>
                            <ActionButton
                                isActive={''}
                                type={'speakBtn'}
                            />
                        </View>

                    </View>
                </View>
            </>
        );
    }
};

