

import React, {Component} from 'react';
import {Platform, Alert, StyleSheet, Text, View, Button, Image, TouchableOpacity, Modal, TextInput } from 'react-native';
import AppButton from '../video/AppButton'
import AVChatConnection from 'react-native-livechatav/av/LivechatAv'


function logi(msg){
  global.calls.log("info", msg);
  return msg;
}


function logw(msg){
  global.calls.log("warn", msg);
  return msg;
}

function loge(msg){
  global.calls.log("error", msg);
  return msg;
}

function logd(msg){
  global.calls.log( "debug", msg);
  return msg;
}


export default class StartScreen extends Component
{
    state = {
        ctlbtn : { txt:"Start", isActive:false, click:this.start },
        auxbtn : { txt:"Call", isActive:false },
        libver : ""
    }

    //libver="";

    constructor(props)
    {
        super(props);

        logi( "StartScreen constructed" );

        this.orgSetState = this.setState;
        this.setState = ( f ) => {
            this.orgSetState( ( s ) =>
                {
                    return global.state.StartScreen = f(s);
                }
            );
        }

        AVChatConnection.getLibVersion( (val)=>
        {
            this.setState( (s) => {
                s.libver = val;
                return s;
            } );
        } );

        if( global.state.StartScreen )
        {
            logi( "Create from global state" );
            this.state = global.state.StartScreen;
        }
        else
        {
            logi( "Create from local state" );
            global.state.StartScreen = this.state;
            this.state.ctlbtn.click = "start";
            this.state.auxbtn.click = "call";
        }

        this.fn = { start:this.start, end:this.end, call:this.call, endcall:this.endcall };
    }


    start = () =>
    {
        logi( "starting.." );
        
        global.calls.createCall().Initialize();

        global.callsession.AddHandler( global.callsession.StateEvent.CONNECTING_MEDIA, "start",
            ( mode ) =>
            {
                global.pushNav( this.props.navigation, "Call" );
            }
        );

        global.callsession.AddHandler(  global.callsession.CommonEvent.CALL_TIME, "start",
            ( time, fmttime ) =>
            {

            }
        );

        global.callsession.AddHandler(  global.callsession.StateEvent.ENDED, "start",
            ( mode ) =>
            {
                this.props.navigation.navigate( 'Start' );
            }
        );

        global.callsession.AddHandler(  global.callsession.StateEvent.INCALL_MEDIA, "start",
            ( mode ) =>
            {
                //logi( "Calling LoudSpeaker ON" );
                global.callsession.LoudSpeaker( true );
            }
        );

        window.livechat2Api.onUserFound = ( userFoundData ) => 
        {
            logi( "onUserFound, invoked with : " + JSON.stringify( userFoundData ) );
            this.call();
            logd( "call initiated" );
        }

        const DEMO_CONTENT = { "name":"RN TestApp","address":"Mobile","pIntent":"51001" };

        window.livechat2Api.start( DEMO_CONTENT );

        this.setState( (s)=> {
            s.ctlbtn = { txt:"End", isActive:true, click: "end" };
            return s;
        } );
    }

    end = () =>
    {
        logi( "ending.." );
        //global.callsession.End( "End by App" );
        window.livechat2Api.end();
        this.setState( (s)=> {
            s.ctlbtn = { txt:"Start", isActive:false, click: "start" };
            return s;
        } );
    }

    call = () =>
    {
        logi( "call.." );

        this.setState( (s)=> {
            s.auxbtn = { txt:"End Call", isActive:true, click: "endcall" };
            return s;
        } );

        global.callsession.StartAV( "video", { id:"testid", title:"Test User" }, false );
    }

    endcall = () =>
    {
        logi( "ending call.." );

        this.setState( (s)=> {
            s.auxbtn = { txt:"Call", isActive:false, click: "call" };
            return s;
        } );

        global.callsession.End( "End by click" );
    }


    clickLog = () =>
    {
        global.pushNav( this.props.navigation , 'Logs' );
    }


    render()
    {
        console.log( "Rendering StartScreen ..." );

        var comp;

        if( this.state.ctlbtn.isActive )
        {
            comp = ( <View>
            <AppButton title={ this.state.auxbtn.txt } onPress={ this.fn[this.state.auxbtn.click] }/>
            </View>
            );
        }

        return (

            <View style={ { flex:1, flexDirection: 'column', justifyContent : "space-evenly", alignItems : "center"} }>

                <View>
                    <AppButton title={ this.state.ctlbtn.txt } onPress={ this.fn[this.state.ctlbtn.click] }/>
                </View>

                { comp }

                <View>
                    <AppButton title="Log" onPress={ this.clickLog }/>
                </View>

                <Text style={{  color:"#aaa0ff", fontSize:12, margin:5 }} > livechatav: v{this.state.libver}</Text>

            </View>
        );
    }
}

