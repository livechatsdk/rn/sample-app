# Sample App



## Getting started


```
git clone https://gitlab.com/livechatsdk/rn/sample-app.git
cd sample-app
npm install
```
#### android
Edit android/local.properties and set sdk
```
npx react-native run-android --variant=release
```

#### ios
```
cd ios
pod install
npx react-native run-ios --configuration=Release
```
