/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);

import CallSesssion, {CallSessionConfig} from 'react-native-livechatav/av/CallSession';

global.userMap = {};

global.SetNameForId = (id, name) => {
  global.userMap[id] = name;
};

global.GetNameFromId = id => {
  return global.userMap[id];
};


global.configs = {
    "av": {
        "type": "configs",
        "ice": [
			{
                "url": "turn:calabrio.tetherfi.cloud:3585",
                "user": "tetherfi",
                "pass": "nuwan"
            }
        ],
        "avmode": "audio",
        "video": {
            "width": 720,
            "height": 720,
            "fps": 25
        },
        "media_conn_mode": "all",
        "mirror_selfview": true,
        "android_manage_audio": true,
        "prompt_avrequest": true,
        "sdp_mung": {
            "enabled": true,
            "audio_limit": 32,
            "video_limit": 128,
            "session_limit": 1024,
            "vpcodec": 9
        },
        "detect_gsm": false,
        //"sdpPlan" : "plan-b",
        "sdpPlan" : "unified-plan",
        //"scaling" : "asfill" // "asfit"
        "scaling" : "asfit"
    }
};

global.callsession = { currentState:{ usertxt:"" } } // backup

global.contact = {
    name : "init",
    id : ""
};

global.state = {};

// global.configs.custinfo = {
// }

global.log = {
    data : [],
    count : 0,
    outfunc : function( type, msg ){ console.log( msg ); },
    func:( type, msg )=>
    {
        global.log.data.push
        (
            {
                key:(global.log.count++).toString(),
                type:type,
                msg: msg
            }
        );

        global.log.outfunc( type, msg );

    }
};

global.calls = {

    callCreateHandlers : {}, // args(callsession)

    createCall : () =>  // returns callsession
    {
        var csconfig = new CallSessionConfig(
            global.configs.sigurl,
            global.configs.custinfo
        );

        if( global.callsession )
        {
            if( global.callsession.release )
            {
                global.callsession.release();
            }
        }

        global.callsession = new CallSesssion( csconfig );

        for( var prop in global.calls.callCreateHandlers )
        {
            if( global.calls.callCreateHandlers.hasOwnProperty(prop) )
            {
                global.calls.callCreateHandlers[prop]( global.callsession );
            }
        }

        global.callsession.SetUserIdResolver ( global.GetNameFromId );

        return global.callsession;

    },

    log:function(type,msg){ console.log( msg ); }
};

global.pushNav = function( nav, to )
{
    if( to == nav.state.routeName )
    {
        return;
    }

    var stack = nav.getParam( 'navstack' );

    if( stack )
    {
        stack.push( nav.state.routeName );
    }
    else
    {
        stack = [ nav.state.routeName ];
    }

    // {meta: global.chatMeta}

    console.log( "Push Nav : " +  nav.state.routeName + " to " + to );

    nav.navigate( to, { navstack : stack } );
}

global.goBack = function( nav )
{
    var stack = nav.getParam( 'navstack' );
    if( stack )
    {
        var screen = stack.pop();
        if( screen )
        {
            console.log( "Nav : " +  screen );
            nav.navigate( screen, {meta: global.chatMeta} );
        }
    }
}

