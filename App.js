/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { StyleSheet, Text, View } from 'react-native';
import CallScreen from './video/CallScreenEx';
import LogScreen from './video/LogScreen'
import StartScreen from './video/StartScreen'

import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {Livechat2Api} from 'react-native-livechatav/sig/livechat2api'

const NavApp = createSwitchNavigator(
    {
            Start : StartScreen,
            Call :  CallScreen,
            Logs : { screen: LogScreen },
            ChatThread : { screen: LogScreen },
            ChatList : { screen: LogScreen }
    },
    {
            //defaultNavigationOptions : { tabBarVisible: false },
            initialRouteName : 'Start',
            //lazy : false
    }
);

const NavAppCont = createAppContainer(NavApp);

global.token = "eyJhbGciOiJIUzUxMiJ9.eyJjaGFubmVsIjoiQ0hBVCIsInNlc3Npb25JZCI6ImFrZHNqYWxza2FTIiwiZW52IjoiREVWIiwicGFnZUlkIjoiQUNDT1VOVFMiLCJtb2JpbGVBdXRoRmxhZyI6InRydWUiLCJjdXN0b21lck5hbWUiOiJ0ZXRoZXJmaSIsIm9yZ0lkIjoiMSIsInNraWxsIjoiQUNDT1VOVFMiLCJvcmdhbml6YXRpb24iOiJ0ZXRoZXJmaSIsImN1c3RvbWVySWQiOiI1NDY3NTEzMDUxIiwiZXhwIjo3NjIxNjUyMTQsImlhdCI6MTY1NDg2MzgzOCwianRpIjoiMmE3NTk3NDUtYjYzNi00ZWQwLThhZWUtMGU3NjNlZGNkZDZlIn0.BBcf-7MzgP40DbD5yibQSrWAqDT_rJk3FMDiMWDs8B9jSmqSysJhn526B6ypMZ_fR48ocdICxbj_v0LnTocgTw&sessionId=3a600342-a7a3-4c66-bbd3-f67de5d7096f&authType=cloud";
global.sigurl = "wss://calabrio.tetherfi.cloud:9443";

global.sigurlocal = "wss://34.142.197.160:8090";

export class TestApp extends React.Component {

  constructor(props)
  {

    super(props);
    window.livechat2Api = new Livechat2Api( global.sigurl/*ocal*/ );
    window.livechat2Api.setUserAuth( "test", global.token );
    window.livechat2Api.sdkLoggingConfigurations( true, true, false );
    window.livechat2Api.log = global.log.func;
    window.livechat2Api.logClientMessage = ( p1, p2 )=>{ }

  }

  render() {
    return (
      <NavAppCont/>
    );
  }

};


const App: () => React$Node = () => {
  return (
    <TestApp/>
  )
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
